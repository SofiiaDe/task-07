package com.epam.rd.java.basic.task7.db;

import java.util.ArrayList;
import java.util.List;
import java.sql.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

import com.epam.rd.java.basic.task7.db.entity.*;

public class DBManager {

    private static DBManager instance;

    private static final String CONNECTION_URL = "jdbc:derby:memory:testdb;create=true";

    private static final String GET_ALL_USERS = "SELECT * FROM users";
    private static final String INSERT_USER = "INSERT INTO users (id, login) VALUES (DEFAULT, ?)";
    private static final String DELETE_USERS = "DELETE FROM users WHERE login=?";
    private static final String GET_USER_BY_LOGIN = "SELECT id, login FROM users WHERE login=?";
    private static final String FIND_ALL_TEAMS = "SELECT * FROM teams";
    private static final String INSERT_TEAM = "INSERT INTO teams (id, name) VALUES (DEFAULT, ?)";
    private static final String INSERT_TEAMS_FOR_USER = "INSERT INTO users_teams (user_id, team_id) VALUES ((?),(?))";
    private static final String GET_TEAM_BY_ID = "SELECT * FROM teams t WHERE t.id=? ORDER BY t.id";
    private static final String GET_USER_TEAMS = "SELECT * FROM users_teams ut WHERE ut.user_id=? ORDER BY team_id";
    private static final String DELETE_TEAM = "DELETE FROM teams WHERE name=?";
    private static final String UPDATE_TEAM = "UPDATE teams SET name =? WHERE id = ?";

    /**
     * singleton
     *
     * @return DBManager object
     */
    public static synchronized DBManager getInstance() {

        if (instance == null) {
            instance = new DBManager();
        }
        return instance;
    }

    private DBManager() {
    }

    public List<User> findAllUsers() throws DBException {

        List<User> allUsersList = new ArrayList<>();
        Connection con = null;
        Statement stmt = null;
        ResultSet rs = null;

        try {
            con = DriverManager.getConnection(CONNECTION_URL);
            stmt = con.createStatement();
            rs = stmt.executeQuery(GET_ALL_USERS);
            while (rs.next()) {
                User user = new User();
                user.setId(rs.getInt("id"));
                user.setLogin(rs.getString("login"));
                allUsersList.add(user);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            close(con);
            close(stmt);
            close(rs);
        }

        return allUsersList;
    }

    public boolean insertUser(User user) throws DBException {

        User.createUser(user.getLogin());

        Connection con = null;
        PreparedStatement pstmt = null;

        try {
            con = DriverManager.getConnection(CONNECTION_URL);
            con.setAutoCommit(false);

            pstmt = con.prepareStatement(INSERT_USER,
                    Statement.RETURN_GENERATED_KEYS);
            pstmt.setString(1, user.getLogin());

            int count = pstmt.executeUpdate();
            if (count > 0) {
                try (ResultSet resultSet = pstmt.getGeneratedKeys()) {
                    if (resultSet.next()) {
                        user.setId(resultSet.getInt(1));
                    }
                }
            }
            con.commit();
        } catch (SQLException e) {
            e.printStackTrace();
            rollBack(con);
        } finally {
            close(con);
            close(pstmt);
        }

        return false;
    }

    public boolean deleteUsers(User... users) throws DBException {

        PreparedStatement pstmt = null;
        Connection con = null;

        try {
            con = DriverManager.getConnection(CONNECTION_URL);

            for (User user : users) {
                pstmt = con.prepareStatement(DELETE_USERS);
                pstmt.setString(1, user.getLogin());
                pstmt.executeUpdate();
            }

        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        } finally {
            close(con);
            close(pstmt);
        }
        return false;
    }

    public User getUser(String login) throws DBException {

        User user = null;
        Connection con = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            con = DriverManager.getConnection(CONNECTION_URL);
            pstmt = con.prepareStatement(GET_USER_BY_LOGIN);
            pstmt.setString(1, login);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                user = new User();
                user.setId(rs.getInt("id"));
                user.setLogin(login);

            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            close(con);
            close(pstmt);
            close(rs);
        }

        return user;
    }

    public Team getTeam(String name) throws DBException {
        Team team = null;
        for (Team teamTmp : findAllTeams()) {
            if (name.equals(teamTmp.getName())) {
                team = teamTmp;
                break;
            }
        }
        return team;
    }

    public List<Team> findAllTeams() throws DBException {

        List<Team> allTeamsList = new ArrayList<>();
        Connection con = null;
        Statement st = null;
        ResultSet rs = null;

        try {
            con = DriverManager.getConnection(CONNECTION_URL);
            st = con.createStatement();
            rs = st.executeQuery(FIND_ALL_TEAMS);
            while (rs.next()) {
                Team team = new Team(rs.getInt("id"), rs.getString("name"));
                allTeamsList.add(team);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            close(con);
            close(st);
            close(rs);
        }

        return allTeamsList;
    }

    public boolean insertTeam(Team team) throws DBException {

        Team.createTeam(team.getName());
        Connection con = null;
        PreparedStatement pstmt = null;

        try {
            con = DriverManager.getConnection(CONNECTION_URL);
            con.setAutoCommit(false);

            pstmt = con.prepareStatement(INSERT_TEAM, Statement.RETURN_GENERATED_KEYS);
            pstmt.setString(1, team.getName());

            int count = pstmt.executeUpdate();
            if (count > 0) {
                try (ResultSet rs = pstmt.getGeneratedKeys();) {
                    if (rs.next()) {
                        team.setId(rs.getInt(1));
                    }
                }
            }
            con.commit();
        } catch (SQLException e) {
            e.printStackTrace();
            rollBack(con);
        } finally {
            close(con);
            close(pstmt);
        }
        return false;
    }

    public boolean setTeamsForUser(User user, Team... teams) throws DBException {

        Connection con = null;

        try {
            con = DriverManager.getConnection(CONNECTION_URL);
            con.setAutoCommit(false);

            for (Team team : teams) {
                try(PreparedStatement pstmt = con.prepareStatement(INSERT_TEAMS_FOR_USER)) {
                    pstmt.setInt(1, user.getId());
                    pstmt.setInt(2, team.getId());
                    pstmt.executeUpdate();
                }

            }
            con.commit();

        } catch (SQLException e) {
            rollBack(con);
            e.printStackTrace();
            throw new DBException("A transaction has been failed", e);
        } finally {
            close(con);
        }

        return false;
    }

    public Team getTeamById(int id) throws DBException {
        Team team = new Team();
        Connection con = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try {
            con = DriverManager.getConnection(CONNECTION_URL);
            pstmt = con.prepareStatement(GET_TEAM_BY_ID);
            pstmt.setInt(1, id);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                team.setId(id);
                team.setName(rs.getString("name"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            close(con);
            close(pstmt);
            close(rs);
        }

        return team;
    }

    public List<Team> getUserTeams(User user) throws DBException {

        List<Team> userTeamsList = new ArrayList<>();

        Connection con = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try {
            con = DriverManager.getConnection(CONNECTION_URL);
            pstmt = con.prepareStatement(GET_USER_TEAMS);
            pstmt.setInt(1, user.getId());
            rs = pstmt.executeQuery();

            while (rs.next()) {
                userTeamsList.add(getTeamById(rs.getInt("team_id")));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            close(con);
            close(pstmt);
            close(rs);
        }

        return userTeamsList;
    }

    public boolean deleteTeam(Team team) throws DBException {

        boolean teamDeleted;
        Connection con = null;
        PreparedStatement statement = null;

        try {
            con = DriverManager.getConnection(CONNECTION_URL);

            statement = con.prepareStatement(DELETE_TEAM);
            statement.setString(1, team.getName());
            teamDeleted = statement.executeUpdate() > 0;

        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        } finally {
            close(con);
            close(statement);
        }
        return teamDeleted;
    }

    public boolean updateTeam(Team team) throws DBException {

        boolean teamUpdated = false;
        Connection con = null;
        PreparedStatement pstmt = null;
        try {
            con = DriverManager.getConnection(CONNECTION_URL);
            pstmt = con.prepareStatement(UPDATE_TEAM);
            pstmt.setString(1, team.getName());
            pstmt.setInt(2, team.getId());
            teamUpdated = pstmt.executeUpdate() > 0;

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            close(con);
            close(pstmt);
        }
        return teamUpdated;
    }

    private void rollBack(Connection connection) {
        if (connection != null) {
            try {
                connection.rollback();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    private void close(AutoCloseable itemToBeClosed) {
        if (itemToBeClosed != null) {
            try {
                itemToBeClosed.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


}
